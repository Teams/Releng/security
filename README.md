# Security

Historically, we have operated a security@gnome.org email alias. However, it
receives tons of spam, and spam wastes everybody's time. Additionally, it is
easy to lose track of outstanding issues with email-based workflows.

To solve this, let's track issues in GitLab instead. There are two ways to bring
security issues to our attention:

 * Use the [web form](https://security.gnome.org/) (recommended for anybody without a GNOME GitLab account)
 * Directly create an report here (only if you have a GNOME GitLab account)
 
When creating an issue report via GitLab, consider marking your report as
confidential. Issues reported via the web form are automatically marked as
confidential.

Since February 2024, GNOME project maintainers are expected to report security
issues in this issue tracker to alert GNOME Security, so the issues can be
documented.

## GPG

GPG sucks. Just use the web form.

## CVE Assignment

GNOME Security generally only assists with CVE assignment for higher severity
issues. Reporters are welcome to request a CVE themselves for lower severity
issues, provided we agree there exists a security issue.

Please write to <secalert@redhat.com> to request a CVE. Requesting a CVE directly
from MITRE is not recommended and may result in significant delays.